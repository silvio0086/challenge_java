package challengejava;

/**
 *
 * @author Silvio
 */
public class Informacion_items {
    private String apodo;
    private String contra;
    private double ventaja;
    private double porcent_vict;

    public Informacion_items(){};
    
    public Informacion_items(String apodo, String contra, double ventaja, double porcent_vict) {
        this.apodo = apodo;
        this.contra = contra;
        this.ventaja = ventaja;
        this.porcent_vict = porcent_vict;
    }

    public String getApodo() {
        return apodo;
    }

    public void setApodo(String apodo) {
        this.apodo = apodo;
    }

    public String getContra() {
        return contra;
    }

    public void setContra(String contra) {
        this.contra = contra;
    }

    public double getVentaja() {
        return ventaja;
    }

    public void setVentaja(double ventaja) {
        this.ventaja = ventaja;
    }

    public double getPorcent_vict() {
        return porcent_vict;
    }

    public void setPorcent_vict(double porcent_vict) {
        this.porcent_vict = porcent_vict;
    }

    @Override
    public String toString() {
        return "Informacion_items{" + "apodo=" + apodo + ", contra=" + contra + ", ventaja=" + ventaja + ", porcent_vict=" + porcent_vict + '}';
    }
    
   
}
