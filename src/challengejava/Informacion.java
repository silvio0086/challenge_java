
package challengejava;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Silvio
 */
public class Informacion {
    private ArrayList <Informacion_items> informacion;

    public Informacion(){};
    
    public Informacion(ArrayList<Informacion_items> informacion) {
        this.informacion = informacion;
    }

    public ArrayList<Informacion_items> getInformacion() {
        return informacion;
    }

    public void setInformacion(ArrayList<Informacion_items> informacion) {
        this.informacion = informacion;
    }
    
    
}
